﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using SQLite;
using NaturApp.Pedidos;
using System.Collections.ObjectModel;

namespace NaturApp.Pagos
{
    public partial class consultaPagoPagos : PhoneApplicationPage
    {
        string idPedido;
        SQLiteConnection db;
        tablaPedidos pedido;

        IEnumerable<tablaPagos> pagos;
        ObservableCollection<cConsultaPagos> arrPagos;

        public consultaPagoPagos()
        {
            InitializeComponent();
            db = new SQLiteConnection("naturapp.db");
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("idPedido"))
                idPedido = NavigationContext.QueryString["idPedido"].ToString();

            pedido = db.Query<tablaPedidos>("SELECT * from tablaPedidos where idPedido like " + "'" + idPedido + "'").FirstOrDefault();
            pagos = db.Query<tablaPagos>("SELECT * from tablaPagos where idPedido like " + "'" + idPedido + "'");

            arrPagos = new ObservableCollection<cConsultaPagos>();

            foreach (var pago in pagos)
            {
                arrPagos.Add(new cConsultaPagos(pedido.producto, pago.fechaPago.ToShortDateString(), pago.cantidadPago.ToString(), pago.idPago.ToString()));
            }

            listPagos.ItemsSource = arrPagos;

            base.OnNavigatedTo(e);
        }

        private void listPagos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var pago = listPagos.SelectedItem as cConsultaPagos;
            int id = Convert.ToInt32(pago.idPago);

            if (pedido != null)
                NavigationService.Navigate(new Uri("/Pagos/editarPagos.xaml?idPago=" + pago.idPago, UriKind.Relative));
        }
    }
}