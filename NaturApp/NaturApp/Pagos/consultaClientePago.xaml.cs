﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using SQLite;
using System.Collections.ObjectModel;
using NaturApp.Clientes;
using NaturApp.Pedidos;

namespace NaturApp.Pagos
{
    public partial class consultaClientePago : PhoneApplicationPage
    {
        SQLiteConnection db;
        IEnumerable<tablaClientes> clientes;
        IEnumerable<tablaPedidos> pedidos;
        ObservableCollection<Cliente> arrClientes;

        string transaccion;

        public consultaClientePago()
        {
            InitializeComponent();
            db = new SQLiteConnection("naturapp.db");
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("transaccion"))
                transaccion = NavigationContext.QueryString["transaccion"].ToString();

            if (transaccion.Equals("crear"))
                txtTitulo.Text = "Selecciona el cliente \n para agregar el pago:";
            else
                if(transaccion.Equals("consultar"))
                    txtTitulo.Text = "Seleccciona el cliente \n para consultar sus pagos:";
                else
                    if(transaccion.Equals("eliminar"))
                        txtTitulo.Text = "Seleccciona el cliente \n para eliminar pago:";
                    else
                        if (transaccion.Equals("editar"))
                            txtTitulo.Text = "Seleccciona el cliente \n para editar pago:";

            var count = (from x in db.Table<tablaClientes>() select x.idCliente).Count();
            if (count > 0)
            {
                clientes = db.Query<tablaClientes>("SELECT idCliente, nombres, apellidos, direccion, telefono, correo, sexo, fechaNacimiento, estadoCivil from tablaClientes WHERE idCliente in (SELECT idCliente FROM tablaPedidos)");
                //clientes = db.Query<tablaClientes>("SELECT * FROM tablaClientes WHERE idCliente IN (SELECT idCliente FROM tablaPedidos");

                if (clientes == null || clientes.Count() == 0)
                {
                    MessageBox.Show("No existen clientes con pedidos");
                }
                else
                {
                    arrClientes = new ObservableCollection<Cliente>();
                    foreach (var cliente in clientes)
                    {
                        arrClientes.Add(new Cliente(cliente.idCliente.ToString(), cliente.nombres, cliente.apellidos, cliente.direccion, cliente.telefono, cliente.correo, cliente.sexo, cliente.fechaNacimiento, cliente.estadoCivil));
                    }

                    listClientes.ItemsSource = arrClientes;
                }
            }
            else
            {
                MessageBox.Show("No hay clientes agregados");
            }

            base.OnNavigatedTo(e);
        }

        private void listClientes_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var cliente = listClientes.SelectedItem as Cliente;

            if (cliente != null)
                NavigationService.Navigate(new Uri("/Pagos/consultaPedidoPagos.xaml?idCliente="+ cliente.id.ToString() + "&transaccion=" + transaccion.ToString(), UriKind.Relative));
        }
    }
}