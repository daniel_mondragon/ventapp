﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using SQLite;
using NaturApp.Pedidos;
using System.Globalization;

namespace NaturApp.Pagos
{
    public partial class editarPagos : PhoneApplicationPage
    {
        string idPago;
        string cantidadPago;
        SQLiteConnection db;
        tablaPedidos pedido;

        tablaPagos pago;

        public editarPagos()
        {
            InitializeComponent();
            db = new SQLiteConnection("naturapp.db");
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("idPago"))
                idPago = NavigationContext.QueryString["idPago"].ToString();

            pago = db.Query<tablaPagos>("SELECT * from tablaPagos where idPago like " + "'" + idPago + "'").FirstOrDefault();


            txtFechaPago.Text = DateTime.Now.ToShortDateString();
            txtPago.Text = pago.cantidadPago.ToString();



            base.OnNavigatedTo(e);
        }

        private void gurdarPago_Click(object sender, RoutedEventArgs e)
        {
            if (txtPago.Text == null || txtPago.Text.ToString().Equals(""))
            {
                MessageBox.Show("El campo 'Cantidad a Pagar' debe tener valor", "Pagos", MessageBoxButton.OK);
            }
            else
            {
                var existing = db.Query<tablaPagos>("SELECT * from tablaPagos where idPago like " + "'" + idPago + "'").FirstOrDefault();
                if (existing != null)
                {
                    existing.fechaPago = DateTime.Now;
                    existing.cantidadPago = Convert.ToDouble(txtPago.Text.ToString(), CultureInfo.InvariantCulture);

                    db.RunInTransaction(() =>
                    {
                        db.Update(existing);
                    });
                }

                MessageBoxResult resultado = MessageBox.Show("Pago actualizado exitosamente", "Pagos", MessageBoxButton.OK);

                NavigationService.GoBack();
            }

        }
    }
}