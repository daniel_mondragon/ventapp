﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace NaturApp.Pagos
{
    public class cPagos
    {
        public string cliente { get; set; }
        public string cantidad { get; set; }
        public string fecha { get; set; }

        public cPagos(string client, string cant, string date)
        {
            this.cliente = client;
            this.cantidad = cant;
            this.fecha = date;
        }
    }

    public class cConsultaPagos
    {
        public string producto { get; set; }
        public string fechaPago { get; set; }
        public string cantidadPago { get; set; }
        public string idPago { get; set; }

        public cConsultaPagos(string product, string fecha, string cantidad, string id)
        {
            this.producto = product;
            this.fechaPago = fecha;
            this.cantidadPago = cantidad;
            this.idPago = id;
        }
    }
}
