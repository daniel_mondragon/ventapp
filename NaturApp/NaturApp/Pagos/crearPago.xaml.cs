﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using SQLite;
using NaturApp.Pedidos;
using System.Collections.ObjectModel;

namespace NaturApp.Pagos
{
    public partial class crearPago : PhoneApplicationPage
    {

        string idPedido;
        string cantidadPago;
        SQLiteConnection db;
        tablaPedidos pedido;

        IEnumerable<tablaPagos> pagos;

        public crearPago()
        {
            InitializeComponent();
            db = new SQLiteConnection("naturapp.db");
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("idPedido"))
                idPedido = NavigationContext.QueryString["idPedido"].ToString();

            pedido = db.Query<tablaPedidos>("SELECT * from tablaPedidos where idPedido like " + "'" + idPedido + "'").FirstOrDefault();
            pagos = db.Query<tablaPagos>("SELECT * from tablaPagos where idPedido like " + "'" + idPedido + "'");

            txtCodigo.Text = pedido.idProducto.ToString();
            txtCantidad.Text = pedido.cantidad.ToString();
            txtFecha.Text = pedido.fechaPedido.ToShortDateString();
            txtPrecio.Text = pedido.precio.ToString();
            txtProducto.Text = pedido.producto.ToString();

            double total = pedido.cantidad * pedido.precio;

            txtTotal.Text = total.ToString();

            txtFechaPago.Text = DateTime.Now.ToShortDateString();

            txtNumeroPagos.Text = pagos.Count().ToString();

            double sumaPagos = 0.0;
            foreach (var pago in pagos)
                sumaPagos += pago.cantidadPago;

            double deuda = pedido.totalPedido - sumaPagos;
            txtDeuda.Text = deuda.ToString();

            base.OnNavigatedTo(e);
        }

        private void gurdarPago_Click(object sender, RoutedEventArgs e)
        {
            if (txtPago.Text == null || txtPago.Text.ToString().Equals(""))
            {
                MessageBox.Show("El campo 'Cantidad a Pagar' debe tener valor", "Pagos", MessageBoxButton.OK);
            }
            else
            {
                db.Insert(new tablaPagos()
                {
                    idCliente = pedido.idCliente,
                    idPedido = pedido.idPedido,
                    cantidadPago = Convert.ToDouble(txtPago.Text.ToString()),
                    fechaPago = pedido.fechaPedido
                });

                MessageBoxResult resultado = MessageBox.Show("Pago generado exitosamente", "Pagos", MessageBoxButton.OK);

                NavigationService.GoBack();
            }

        }
    }
}