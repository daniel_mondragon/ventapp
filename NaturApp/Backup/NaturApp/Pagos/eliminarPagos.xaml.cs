﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using SQLite;
using NaturApp.Pedidos;
using System.Collections.ObjectModel;

namespace NaturApp.Pagos
{
    public partial class eliminarPagos : PhoneApplicationPage
    {
        string idPedido;
        SQLiteConnection db;
        tablaPedidos pedido;

        IEnumerable<tablaPagos> pagos;
        ObservableCollection<cConsultaPagos> arrPagos;

        public eliminarPagos()
        {
            InitializeComponent();
            db = new SQLiteConnection("naturapp.db");
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("idPedido"))
                idPedido = NavigationContext.QueryString["idPedido"].ToString();

            pedido = db.Query<tablaPedidos>("SELECT * from tablaPedidos where idPedido like " + "'" + idPedido + "'").FirstOrDefault();
            pagos = db.Query<tablaPagos>("SELECT * from tablaPagos where idPedido like " + "'" + idPedido + "'");

            arrPagos = new ObservableCollection<cConsultaPagos>();

            foreach (var pago in pagos)
            {
                arrPagos.Add(new cConsultaPagos(pedido.producto, pago.fechaPago.ToShortDateString(), pago.cantidadPago.ToString(), pago.idPago.ToString()));
            }

            listPagos.ItemsSource = arrPagos;

            base.OnNavigatedTo(e);
        }

        private void listPagos_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var pago = listPagos.SelectedItem as cConsultaPagos;
            int id = Convert.ToInt32(pago.idPago);

            MessageBoxResult resultado = MessageBox.Show("¿Seguro que deseas eliminar este pago?", "Pagos", MessageBoxButton.OKCancel);

            if (resultado == MessageBoxResult.OK)
            {
                var pagoEliminar = db.Query<tablaPagos>("SELECT * FROM tablaPagos WHERE idPago LIKE " + pago.idPago).FirstOrDefault();
                if (pagoEliminar != null)
                {
                    db.RunInTransaction(() =>
                    {
                        db.Delete(pagoEliminar);
                    });
                }

                MessageBoxResult m = MessageBox.Show("Pago eliminado satisfactoriamente", "Confirmación", MessageBoxButton.OK);

                if (m == MessageBoxResult.OK)
                {
                    NavigationService.GoBack();
                }
            }
        }
    }
}