﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using SQLite;
using System.Globalization;

namespace NaturApp.Pedidos
{
    public partial class editarPedido : PhoneApplicationPage
    {
        SQLiteConnection db;
        tablaPedidos pedido;

        string idPedido;
        int idProducto;
        string nombreProducto;
        double precio;
        int cantidad;
        double total;
        int idCliente;
        DateTime fecha;

        public editarPedido()
        {
            InitializeComponent();
            db = new SQLiteConnection("naturapp.db");
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("idPedido"))
                idPedido = NavigationContext.QueryString["idPedido"].ToString();

            pedido = db.Query<tablaPedidos>("SELECT * from tablaPedidos where idPedido like " + "'" + idPedido + "'").FirstOrDefault();

            txtCodigo.Text = pedido.idProducto.ToString();
            txtProducto.Text = pedido.producto.ToString();
            txtCantidad.Text = pedido.cantidad.ToString();
            txtPrecio.Text = pedido.precio.ToString();
            txtTotal.Text = pedido.totalPedido.ToString();

            base.OnNavigatedTo(e);
        }

        private void guardar_Click(object sender, RoutedEventArgs e)
        {
            idProducto = Convert.ToInt32(txtCodigo.Text.ToString());
            nombreProducto = txtProducto.Text;
            precio = Convert.ToDouble(txtPrecio.Text.ToString());
            cantidad = Convert.ToInt32(txtCantidad.Text.ToString());
            total = Convert.ToDouble(txtTotal.Text.ToString());
            fecha = DateTime.Now;

            var existing = db.Query<tablaPedidos>("SELECT * from tablaPedidos where idPedido like " + "'" + idPedido + "'").FirstOrDefault();
            if (existing != null)
            {
                existing.idProducto = idProducto;
                existing.producto = nombreProducto;
                existing.precio = precio;
                existing.cantidad = cantidad;
                existing.totalPedido = total;
                existing.fechaPedido = fecha;

                db.RunInTransaction(() =>
                {
                    db.Update(existing);
                });
            }

            MessageBoxResult resultado = MessageBox.Show("El pedido se actualizó con éxito", "Pedidos", MessageBoxButton.OK);

            if (resultado == MessageBoxResult.OK)
            {
                NavigationService.GoBack();
            }
        }

        private void txtCantidad_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!txtPrecio.Text.ToString().Equals("") && !txtCantidad.Text.ToString().Equals(""))
            {
                double precio = Convert.ToDouble(txtPrecio.Text.ToString(), CultureInfo.InvariantCulture);
                int cantidad = Convert.ToInt32(txtCantidad.Text.ToString());
                double total = precio * cantidad;
                txtTotal.Text = total.ToString();
            }
        }
    }
}