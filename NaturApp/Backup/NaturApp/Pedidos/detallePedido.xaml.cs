﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using SQLite;

namespace NaturApp.Pedidos
{
    public partial class detallePedido : PhoneApplicationPage
    {
        string idPedido;
        SQLiteConnection db;
        tablaPedidos pedido;

        public detallePedido()
        {
            InitializeComponent();
            db = new SQLiteConnection("naturapp.db");
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (NavigationContext.QueryString.ContainsKey("idPedido"))
                idPedido = NavigationContext.QueryString["idPedido"].ToString();

            pedido = db.Query<tablaPedidos>("SELECT * from tablaPedidos where idPedido like " + "'" + idPedido + "'").FirstOrDefault();

            txtCodigo.Text = pedido.idProducto.ToString();
            txtCantidad.Text = pedido.cantidad.ToString();
            txtFecha.Text = pedido.fechaPedido.ToShortDateString();
            txtPrecio.Text = pedido.precio.ToString();
            txtProducto.Text = pedido.producto.ToString();

            double total = pedido.cantidad * pedido.precio;

            txtTotal.Text = total.ToString();
            
            base.OnNavigatedTo(e);
        }

        private void btnEditar_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {

        }

        private void btnEliminar_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            MessageBoxResult resultado = MessageBox.Show("¿Seguro que deseas eliminar este pedido?", "Confirmación", MessageBoxButton.OKCancel);
            if (resultado == MessageBoxResult.OK)
            {
                //clienteEliminar = db.Query<tablaClientes>("SELECT idCliente, nombres, apellidos, direccion, telefono, correo, sexo, fechaNacimiento, estadoCivil FROM tablaClientes WHERE idCliente LIKE " + cliente.id).FirstOrDefault();

                if (pedido != null)
                {
                    db.RunInTransaction(() =>
                    {
                        db.Delete(pedido);
                    });
                }

                MessageBoxResult m = MessageBox.Show("Pedido eliminado satisfactoriamente", "Confirmación", MessageBoxButton.OK);

                if (m == MessageBoxResult.OK)
                {
                    NavigationService.GoBack();
                }
            }
        }
    }
}